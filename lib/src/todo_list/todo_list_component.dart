import 'dart:async';
import 'dart:convert';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';

import 'todo_list_service.dart';

@Component(
  selector: 'todo-list',
  styleUrls: const ['todo_list_component.css'],
  templateUrl: 'todo_list_component.html',
  directives: const [
    CORE_DIRECTIVES,
    materialDirectives,
  ],
  providers: const [TodoListService],
)
class TodoListComponent implements OnInit {

  String ip = 'http://192.168.1.145:7891';
  String status = 'Welcome';

  @override
  Future<Null> ngOnInit() async {

  }

  void toggle(bool checked, bool auto, bool on) {
    if (!checked)
      return;
    status = 'Sending...';
    _send(auto, on);
  }

  void _send(bool auto, bool on) {
    var request = new HttpRequest();
    request.open('POST', ip);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.onReadyStateChange.listen((event) {
      if (request.readyState == HttpRequest.DONE && request.status == 200) {
        if (request.response.toString().contains('Hello world'))
          status = 'Ok';
        else
          status = 'State unknown';
      } else if (request.readyState == HttpRequest.DONE && request.status == 0) {
        // Status is 0; most likely the server isn't running.
        status = 'Server not found';
      }
    });
    request.send(utf8.encode(json.encode({"auto":auto, "on":on})));
  }
}
