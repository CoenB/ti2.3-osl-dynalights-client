import 'dart:async';
import 'dart:convert';
import 'dart:html';

import 'package:angular/core.dart';

/// Mock service emulating access to a to-do list stored on a server.
@Injectable()
class TodoListService {
  var path = 'portmanteaux.json';
  bool failed;

//  Future push(List<String> list) async {
//    await HttpRequest.postFormData(path, {"data":json.encode(list)});
//  }

  Future<List<String>> makeRequest() async {
    failed = false;
    try {
      return processString(await HttpRequest.getString(path));
    } catch (e) {
      print('Couldn\'t open $path');
      handleError(e);
      return [];
    }
  }

  List<String> processString(String jsonString) {
    return json.decode(jsonString);
//    List<String> portmanteaux = json.decode(jsonString);
//    for (int i = 0; i < portmanteaux.length; i++) {
//      wordList.children.add(new LIElement()..text = portmanteaux[i]);
//    }
  }

  handleError(Object error) {
    failed = true;
    //wordList.children.add(new LIElement()..text = 'Request failed.');
  }
}

Iterable<String> thingsTodo() sync* {
  var actions = ['Walk', 'Wash', 'Feed'];
  var pets = ['cats', 'dogs'];

  for (var action in actions) {
    for (var pet in pets) {
      if (pet == 'cats' && action != 'Feed') continue;
      yield '$action the $pet';
    }
  }
}
